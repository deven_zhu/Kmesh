module openeuler.io/mesh

go 1.16

require (
	github.com/cilium/ebpf v0.8.0
	github.com/envoyproxy/go-control-plane v0.10.1
	github.com/golang/protobuf v1.5.2
	github.com/google/martian v2.1.0+incompatible
	github.com/jonboulle/clockwork v0.2.2 // indirect
	github.com/lestrrat-go/file-rotatelogs v2.4.0+incompatible
	github.com/lestrrat-go/strftime v1.0.5 // indirect
	github.com/sirupsen/logrus v1.8.1
	golang.org/x/time v0.0.0-20210723032227-1f47c861a9ac
	google.golang.org/grpc v1.36.0
	google.golang.org/protobuf v1.27.1
	k8s.io/api v0.22.3
	k8s.io/apimachinery v0.22.3
	k8s.io/client-go v0.22.3
	sigs.k8s.io/yaml v1.2.0
)
